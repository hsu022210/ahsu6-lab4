package cs601.hotelapp;

import java.nio.file.Path;
import java.util.*;


public class Address{


    private String city;
    private String state;
    private String streetAddress;
    private double latitude;
    private double longitude;


    public Address(String city, String state, String streetAddress, double lat,
			double lon){
        this.city = city;
        this.state = state;
        this.streetAddress = streetAddress;
        this.latitude = lat;
        this.longitude = lon;
    }


//TODO: need getter or setter methods


    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }


    public String getState(){
        return state;
    }

    public void setState(String state){
        this.state = state;
    }


    public String getStreetAddress(){
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress){
        this.streetAddress = streetAddress;
    }


    public double getLatitude(){
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }


    public double getLongitude(){
        return longitude;
    }

    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

}
