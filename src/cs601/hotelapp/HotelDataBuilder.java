package cs601.hotelapp;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.SocketHandler;

/**
 * Created by Alec on 9/28/2016.
 *
 * Class HotelDataBuilder - a class contains loadHotelInfo and loadReviews mehtods.
 */
public class HotelDataBuilder {

    private ThreadSafeHotelData data;

    /**
     * Constructor 1.
     */
    public HotelDataBuilder(ThreadSafeHotelData data){
        this.data = data;
    }


    /**
     * Read the json file with information about the hotels (id, name, address,
     * etc) and load it into the appropriate data structure(s). Note: This
     * method does not load reviews
     *
     * @param filename
     *            the name of the json file that contains information about the
     *            hotels
     */
    public void loadHotelInfo(String jsonFilename) {

        // Hint: Use JSONParser from JSONSimple library
        // FILL IN CODE

        //call addHotels method based on jspn files

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader(jsonFilename));

            JSONObject jsonObject = (JSONObject) obj;

            JSONArray hotelResults = (JSONArray) jsonObject.get("sr");

            for (int i=0; i < hotelResults.size(); i++){

                JSONObject singleHotel = (JSONObject) hotelResults.get(i);

                String hotelId = (String) singleHotel.get("id");
                String hotelName = (String) singleHotel.get("f");
                String city = (String) singleHotel.get("ci");
                String state = (String) singleHotel.get("pr");
                String streetAddress = (String) singleHotel.get("ad");

                JSONObject latAndLon = (JSONObject) singleHotel.get("ll");

                String lat = (String) latAndLon.get("lat");
                String lon = (String) latAndLon.get("lng");

                double latTypeDouble = Double.parseDouble(lat);
                double lonTypeDouble = Double.parseDouble(lon);

                data.addHotel(hotelId, hotelName, city, state, streetAddress, latTypeDouble, lonTypeDouble);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Load reviews for all the hotels into the appropriate data structure(s).
     * Traverse a given directory recursively to find all the json files with
     * reviews and load reviews from each json. Note: this method must be
     * recursive and use DirectoryStream as discussed in class.
     *
     * @param path
     *            the path to the directory that contains json files with
     *            reviews Note that the directory can contain json files, as
     *            well as subfolders (of subfolders etc..) with more json files
     */
    public synchronized void loadReviews(Path path) {
        // FILL IN CODE

        // Hint: first, write a separate method to read a single json file with
        // reviews
        // using JSONSimple library
        // Call this method from this one as you traverse directories and find
        // json files

        try {
            DirectoryStream<Path> pathsList = Files.newDirectoryStream(path);

            for (Path each_path : pathsList) {

                if (Files.isDirectory(each_path)){

                    loadReviews(each_path);

                }else{

                    data.parseSingleJsonFile(each_path.toString());
//                    q.execute(new ReviewMinion(each_path));

                }
            }
        } catch (IOException e) {
            System.out.println("IOException occurred");
            e.printStackTrace();

        }

    }

    public void fetchAttractions(int radiusInMiles) {
//        List<String> partialQueryList = data.generateQueries();
        TreeMap<String, String> partialQueryMap = data.generateQueries();
        String s = "";

        for (String hotelIdForEachQuery:partialQueryMap.keySet()){

            // let's say we want to find out USF's coordinates:
            String urlString = "https://maps.googleapis.com//maps/api/place/textsearch/json?";// &sensor=false";
            urlString += partialQueryMap.get(hotelIdForEachQuery);
            urlString += "&" + "radius=" + radiusInMiles*1609;
            URL url;
            PrintWriter out = null;
            BufferedReader in = null;
            SSLSocket socket = null;
            try {
                url = new URL(urlString);

                SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

                // HTTPS uses port 443
                socket = (SSLSocket) factory.createSocket(url.getHost(), 443);

                // output stream for the secure socket
                out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
                String request = getRequest(url.getHost(), url.getPath() + "?" + url.getQuery());
                System.out.println("Request: " + request);

                out.println(request); // send a request to the server
                out.flush();

                // input stream for the secure socket.
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                // use input stream to read server's response
                String line;
                StringBuffer sb = new StringBuffer();

                String status = "header";
                while ((line = in.readLine()) != null) {
                    if(status == "body"){
//                        System.out.println(line);
                        sb.append(line);
                    }

                    if (line.contains("Connection: close")){
                        status = "body";
                    }
                }

                s = sb.toString();

                JSONParser parser = new JSONParser();

                try{
                    JSONObject obj = (JSONObject) parser.parse(s);

                    JSONObject jsonObject = obj;

                    JSONArray attractionsResults = (JSONArray) jsonObject.get("results");

                    for (int i=0; i < attractionsResults.size(); i++){

                        JSONObject singleAttraction = (JSONObject) attractionsResults.get(i);

                        String attractionId = (String) singleAttraction.get("id");
                        String name = (String) singleAttraction.get("name");
                        Double rating = (Double) singleAttraction.get("rating");
                        String address = (String) singleAttraction.get("formatted_address");

                        if (rating == null){
                            rating = 0.0;
                        }

                        data.addAttraction(attractionId, name, rating, address, hotelIdForEachQuery);

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                System.out.println(
                        "An IOException occured while writing to the socket stream or reading from the stream: " + e);
            } finally {
                try {
                    // close the streams and the socket
                    out.close();
                    in.close();
                    socket.close();
                } catch (IOException e) {
                    System.out.println("An exception occured while trying to close the streams or the socket: " + e);
                }
            }

        }

    }


    private static String getRequest(String host, String pathResourceQuery) {
        String request = "GET " + pathResourceQuery + " HTTP/1.1" + System.lineSeparator() // GET
                // request
                + "Host: " + host + System.lineSeparator() // Host header required for HTTP/1.1
                + "Connection: close" + System.lineSeparator() // make sure the server closes the
                // connection after we fetch one page
                + System.lineSeparator();
        return request;
    }


    public void shutdown(){};

}
