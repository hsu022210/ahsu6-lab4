package cs601.hotelapp;

import java.awt.*;
import java.io.*;
import java.nio.file.Path;
import java.util.*;

import java.nio.file.Files;
import java.nio.file.DirectoryStream;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import static java.nio.file.StandardOpenOption.*;


/**
 * Class ThreadSafeHotelData  - a data structure that stores information about hotels and
 * hotel reviews. Allows to quickly lookup a hotel given the hotel id.
 * Allows to easily find hotel reviews for a given hotel, given the hotelID.
 * Reviews for a given hotel id are sorted by the date and user nickname.
 *
 */
public class ThreadSafeHotelData  {

	// FILL IN CODE - declare data structures to store hotel data
	private TreeMap hotelInfoMap;
	private TreeMap hotelReviewMap;
	private TreeSet reviewTreeSet;
	private TreeMap touristAttractionMap;
	private TreeMap nearHotelAttractionIdMap;

	/**
	 * Default constructor.
	 */
	 
	 //TODO: just initialize instance variables here, instead of creating new data structures
	public ThreadSafeHotelData () {
		// Initialize all data structures
		// FILL IN CODE

		this.hotelInfoMap = new TreeMap<String, Hotel>();
		this.hotelReviewMap = new TreeMap<String, TreeSet>();
		this.reviewTreeSet = new TreeSet<Review>();
		this.touristAttractionMap = new TreeMap<String, TouristAttraction>();
		this.nearHotelAttractionIdMap = new TreeMap<String, TreeSet>();

	}

	/**
	 * Create a Hotel given the parameters, and add it to the appropriate data
	 * structure(s).
	 *
	 * @param hotelId
	 *            - the id of the hotel
	 * @param hotelName
	 *            - the name of the hotel
	 * @param city
	 *            - the city where the hotel is located
	 * @param state
	 *            - the state where the hotel is located.
	 * @param streetAddress
	 *            - the building number and the street
	 * @param latitude
	 * @param longitude
	 */
	public void addHotel(String hotelId, String hotelName, String city, String state, String streetAddress, double lat,
			double lon) {
		// FILL IN CODE

		Hotel hotel = new Hotel(hotelId, hotelName, city, state, streetAddress, lat, lon);
		this.hotelInfoMap.put(hotelId, hotel);

	}

	/**
	 * Add a new review.
	 *
	 * @param hotelId
	 *            - the id of the hotel reviewed
	 * @param reviewId
	 *            - the id of the review
	 * @param rating
	 *            - integer rating 1-5.
	 * @param reviewTitle
	 *            - the title of the review
	 * @param review
	 *            - text of the review
	 * @param isRecommended
	 *            - whether the user recommends it or not
	 * @param date
	 *            - date of the review in the format yyyy-MM-dd, e.g.
	 *            2016-08-29.
	 * @param username
	 *            - the nickname of the user writing the review.
	 * @return true if successful, false if unsuccessful because of invalid date
	 *         or rating. Needs to catch and handle ParseException if the date is invalid.
	 *         Needs to check whether the rating is in the correct range
	 */
	public boolean addReview(String hotelId, String reviewId, int rating, String reviewTitle, String review,
			boolean isRecom, String date, String username) {

		// FILL IN CODE

		Review singleReview;

//		if (getHotels().contains(hotelId) == false){
//			return false;
//		}

		if(date.matches("\\d{4}-\\d{2}-\\d{2}") == false){
			return false;
		}

		if (rating<=5 && rating>=1){
			singleReview = new Review(hotelId, reviewId, rating, reviewTitle, review, isRecom, date, username);

			this.reviewTreeSet.add(singleReview);

			TreeSet temp = (TreeSet) this.reviewTreeSet.clone();

			this.hotelReviewMap.put(hotelId, temp);

			return true;
		}

		return false; // don't forget to change it
	}


	/**
	 * Return an alphabetized list of the ids of all hotels
	 *
	 * @return
	 */
	public List<String> getHotels() {
		// FILL IN CODE

		List<String> idList = new ArrayList<String>(this.hotelInfoMap.keySet());

		return idList; // don't forget to change it
	}


	public boolean getBooleanForIsRecom(String isRecomString){
		if (isRecomString == "YES"){
			return true;
		}else {
			return false;
		}
	}


	public void parseSingleJsonFile(String jsonFilename){

		JSONParser parser = new JSONParser();

		try {

			Object obj = parser.parse(new FileReader(jsonFilename));

			JSONObject jsonObject = (JSONObject) obj;

			JSONObject reviewDetails = (JSONObject) jsonObject.get("reviewDetails");
			JSONObject reviewCollection = (JSONObject) reviewDetails.get("reviewCollection");
			JSONArray reviewList = (JSONArray) reviewCollection.get("review");

			for (int i=0; i < reviewList.size(); i++){

				JSONObject singleReview = (JSONObject) reviewList.get(i);

				String hotelId = (String) singleReview.get("hotelId");

				String reviewId = (String) singleReview.get("reviewId");

				long ratingLong = (long) singleReview.get("ratingOverall");
				int rating = (int) ratingLong;

				String reviewTitle = (String) singleReview.get("title");
				String review = (String) singleReview.get("reviewText");

				String isRecomString = (String) singleReview.get("isRecommended");
				boolean isRecom = getBooleanForIsRecom(isRecomString);

				String rawDate = (String) singleReview.get("reviewSubmissionTime");
				String[] dateSplitList = rawDate.split("T");
				String date = dateSplitList[0];

				String username = (String) singleReview.get("userNickname");

				if (username.equals("")){
					username = "anonymous";
				}

				boolean addReviewResult = addReview(hotelId, reviewId, rating, reviewTitle, review, isRecom, date, username);

			}

			this.reviewTreeSet.clear();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Returns a string representing information about the hotel with the given
	 * id, including all the reviews for this hotel separated by
	 * -------------------- Format of the string: HoteName: hotelId
	 * streetAddress city, state -------------------- Review by username: rating
	 * ReviewTitle ReviewText -------------------- Review by username: rating
	 * ReviewTitle ReviewText ...
	 *
	 * @param hotel
	 *            id
	 * @return - output string.
	 */
	public String toString(String hotelId) {

		// FILL IN CODE

		if (getHotels().contains(hotelId) == false){
			return "";
		}

		Hotel hotelInfo = (Hotel) this.hotelInfoMap.get(hotelId);

		TreeSet<Review> hotelReview = (TreeSet<Review>) this.hotelReviewMap.get(hotelId);

		String result;
		String newLine = "\n";

		Address hotelAddress = hotelInfo.getAddress();

		result = hotelInfo.getHotelName() + ": " + hotelInfo.getHotelId();
		result += newLine;

		result += hotelAddress.getStreetAddress();
		result += newLine;

		result += hotelAddress.getCity() + ", " + hotelAddress.getState();
		result += newLine;

		if (hotelReview == null){
			return result;
		}

		for (Review r : hotelReview){

			result += "--------------------";
			result += newLine;

			String userName = r.getUsername();

			result += "Review by " + userName + ": " + r.getRating();
			result += newLine;

			result += r.getReviewTitle();
			result += newLine;

			result += r.getReview();
			result += newLine;

		}

		return result; // don't forget to change to the correct string
	}


	/**
	 * Save the string representation of the hotel data to the file specified by
	 * filename in the following format:
	 * an empty line
	 * A line of 20 asterisks ******************** on the next line
	 * information for each hotel, printed in the format described in the toString method of this class.
	 *
	 * @param filename
	 *            - Path specifying where to save the output.
	 */
	public void printToFile(Path filename) {
		// FILL IN CODE

		List<String> hotelIdList = getHotels();

		try (OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(filename, CREATE, APPEND))) {

			for (String id: hotelIdList){
				String result = "\n" + "********************" + "\n";
				result += toString(id);
				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}

		} catch (IOException x) {
			System.err.println(x);
		}
	}


	public void addAttraction(String attractionId, String name, double
			rating, String address, String hotelId){
		TouristAttraction TA = new TouristAttraction(attractionId, name, address, rating);

		this.touristAttractionMap.put(attractionId, TA);

		TreeSet attractionIdSet;

		if (this.nearHotelAttractionIdMap.containsKey(hotelId)){
			attractionIdSet = (TreeSet) this.nearHotelAttractionIdMap.get(hotelId);
		}else{
			attractionIdSet = new TreeSet <String>();
		}
		attractionIdSet.add(attractionId);

		this.nearHotelAttractionIdMap.put(hotelId, attractionIdSet);
	}


	public String getAttractions(String hotelId){
		String newLine = "\n";
		Hotel hotel = (Hotel)this.hotelInfoMap.get(hotelId);

		String result = "Attractions near " + hotel.getHotelName() + ", " + hotelId;
		result += newLine;

		TreeSet<String> attractions = (TreeSet)this.nearHotelAttractionIdMap.get(hotelId);

		if (attractions == null){
			return result;
		}

		for(String attractionId :attractions){
			TouristAttraction attraction = (TouristAttraction) this.touristAttractionMap.get(attractionId);
			result += attraction.getName() + "; " + attraction.getAddress();
			result += newLine;
		}

		return result;
	}

	public void printAttractionsNearEachHotel(Path filename){
		List<String> idList = getHotels();

//		for (String eachHotelId:idList){
//			System.out.print(getAttractions(eachHotelId));
//			System.out.println("++++++++++++++++++++");
//		}

		try (OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(filename, CREATE, APPEND))) {

			for (String eachHotelId:idList){
				String result = getAttractions(eachHotelId);
				result += "\n";
				result += "++++++++++++++++++++";
				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}

		} catch (IOException x) {
			System.err.println(x);
		}
	}

	public TreeMap generateQueries(){

//		List<String> partialQueryList = new ArrayList<>();

		TreeMap<String, String> partialQueryMap = new TreeMap<>();

		Collection<Hotel> hotels = this.hotelInfoMap.values();

		for(Hotel hotel:hotels ){
			String result = "query=tourist%20attractions+in+" + hotel.getAddress().getCity();
			result += "&" + "location=" + hotel.getAddress().getLatitude() + "," + hotel.getAddress().getLongitude();
			result += "&" + "key=" + "AIzaSyD26azAtmPWLf4g_pVSLaHoHEX2UJ5-Bns";
//			partialQueryList.add(result);
			partialQueryMap.put(hotel.getHotelId(), result);
		}
//		return partialQueryList;
		return partialQueryMap;
	}
}
